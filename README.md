<img src="assets/logo_crop.png" height="200px" />

# Yet Another Cute Animal API

> The last cat/dog/capybara/cuteness API you'll ever need. 🐱✨

A simple API for getting cute animal images via an HTTP GET request.

Designed to be simple and easy to use, with no authentication required. Just send a GET request to the API endpoint and you'll get a random cute animal image in response.

You can either host your own instance of the API, or use the public instance hosted at [cute-api.zue.dev](https://cute-api.zue.dev).
