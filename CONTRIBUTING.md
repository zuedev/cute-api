# Contributing Guidelines

We're always looking for contributors to help us improve the API. You can contribute by either adding more cute animals to our database or by improving the API's code itself.

## Adding more cute animals

All cute animal image submissions must adhere to the following:

- The directory name must be a random alphanumeric string with a minimum length of 10 characters
- The image must be in the public domain
- The image must be a PNG file
- The image must be accompanied by a **JSON file** with the following minimum requirements:
  - `source`: The URL of the image source
- Both the image and the JSON file must be named "entry.png" and "entry.json" respectively

Apart from the above requirements, please follow the existing directory structure when adding new images.

Once you're ready to submit your cute animal image, please follow the [fork-and-merge](#fork-and-merge-model) model detailed below.

## Improving the Project

We're open to any and all project improvements, from code refactoring to adding new features and even documentation edits.

If you're interested in improving the project, please follow the [fork-and-merge](#fork-and-merge-model) model detailed below.

## Fork-and-merge model

1. Fork the repo on GitLab
2. Clone the project to your own machine
3. Commit changes to your own branch matching the following naming convention: `username/branch-name`
4. Push your work back up to your fork
5. Submit a merge request so that we can review your changes
6. ???
7. Profit
