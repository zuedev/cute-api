import "dotenv/config";

import * as Sentry from "@sentry/node";
import { ProfilingIntegration } from "@sentry/profiling-node";

import express from "express";

import cute from "./library/cute.js";
import getGitHash from "./library/getGitHash.js";

const app = express();

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  environment: process.env.ENVIRONMENT,
  integrations: [
    new Sentry.Integrations.Http({ tracing: true }),
    new Sentry.Integrations.Express({ app }),
    new ProfilingIntegration(),
  ],
  tracesSampleRate: 1.0,
  profilesSampleRate: 1.0,
});

app.use(Sentry.Handlers.requestHandler());

app.use(Sentry.Handlers.tracingHandler());

app.get("/debug-sentry", function mainHandler(request, response) {
  throw new Error("My first Sentry error!");
});

app.get("/", (request, response) => {
  response.sendFile("index.html", { root: "src" });
});

app.get("/version", async (request, response) => {
  const version = getGitHash();

  response
    .send({
      version,
    })
    .end();
});

app.get("/:animal", (request, response) => {
  const { animal } = request.params;

  response.redirect(`/${animal}/random.png`);
});

app.get("/:animal/:key.:format", (request, response) => {
  try {
    const { animal, key, format } = request.params;

    const cuteData = cute({ animal, key, format });

    if (cuteData.error) return response.status(cuteData.error).send(cuteData);

    if (animal === "random" || key === "random") {
      const { animal: chosenAnimal, key: chosenKey } = cuteData.parameters;

      return response.redirect(`/${chosenAnimal}/${chosenKey}.${format}`);
    }

    if (cuteData.ContentType)
      response.set("Content-Type", cuteData.ContentType);

    response.send(cuteData.data).end();
  } catch (error) {
    console.error(error);

    response.status(500).send("Something unexpected went wrong!").end();
  }
});

app.use(Sentry.Handlers.errorHandler());

app.listen(3000, () => {
  console.log("Server started on port 3000");
});
