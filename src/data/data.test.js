import fs from "fs";

/**
 * This test checks that all data files adhere to the following structure:
 * - src/data/{animal}/{entry}/entry.png
 * - src/data/{animal}/{entry}/entry.json
 *
 * This test is not meant to be run in CI, but rather to be run locally before committing.
 */
test("data/* sanity checks", () => {
  const animalDirectories = fs
    .readdirSync("src/data", { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);

  for (const animalDirectory of animalDirectories) {
    const entryDirectories = fs
      .readdirSync(`src/data/${animalDirectory}`, { withFileTypes: true })
      .filter((dirent) => dirent.isDirectory())
      .map((dirent) => dirent.name);

    for (const entryDirectory of entryDirectories) {
      expect(
        fs.existsSync(`src/data/${animalDirectory}/${entryDirectory}/entry.png`)
      ).toBe(true);
      expect(
        fs.existsSync(
          `src/data/${animalDirectory}/${entryDirectory}/entry.json`
        )
      ).toBe(true);
    }
  }
});
