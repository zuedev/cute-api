import { execSync } from "child_process";

/**
 * Get the current git hash.
 *
 * @param {boolean} short - Whether to return the short hash.
 *
 * @returns {string} The current git hash.
 */
export default (short = false) => {
  const command = `git rev-parse ${short ? "--short" : ""} HEAD`;

  const gitHash = execSync(command).toString().trim();

  return gitHash;
};
