import fs from "fs";

/**
 * Gets an animal from the "database."
 *
 * @param {string} animal The animal to get. Set to "random" to get a random animal.
 * @param {string} key The key of the animal to get. Set to "random" to get a random key.
 * @param {boolean} json Whether or not to return the kitty as a JSON object instead.
 */
export default ({ animal = "random", key = "random", format = "png" }) => {
  // all params should be alphanumeric strings only
  if (
    !/^[a-z0-9]+$/i.test(animal) ||
    !/^[a-z0-9]+$/i.test(key) ||
    !/^[a-z0-9]+$/i.test(format)
  )
    return {
      error: 400,
      message: "Invalid characters in animal or key",
      debug: {
        animal,
        key,
        format,
      },
    };

  // get all the animal subdirectories from data
  const animalDirectories = fs
    .readdirSync("src/data", { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);

  // what animal do we want?
  let chosenAnimal;

  // if the animal is random, choose a random animal
  if (animal === "random") {
    chosenAnimal =
      animalDirectories[Math.floor(Math.random() * animalDirectories.length)];
  } else {
    // otherwise, use the animal
    chosenAnimal = animal;
  }

  // does the chosen animal exist?
  if (!animalDirectories.includes(chosenAnimal))
    return {
      error: 404,
      message: "That animal does not exist!",
      debug: {
        animal,
      },
    };

  // get all the subdirectories from chosen animal
  const chosenAnimalDirectories = fs
    .readdirSync(`src/data/${chosenAnimal}`, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);

  // what animal key do we want?
  let chosenOne;

  // if the key is random, choose a random key
  if (key === "random") {
    chosenOne =
      chosenAnimalDirectories[
        Math.floor(Math.random() * chosenAnimalDirectories.length)
      ];
  } else {
    // otherwise, use the key
    chosenOne = key;
  }

  // does the chosen key exist?
  if (!chosenAnimalDirectories.includes(chosenOne))
    return {
      error: 404,
      message: "That key does not exist!",
      debug: {
        animal,
        key,
      },
    };

  // what file extension do we want?
  switch (format) {
    case "png":
      return {
        ContentType: "image/png",
        data: fs.readFileSync(
          `src/data/${chosenAnimal}/${chosenOne}/entry.png`
        ),
        parameters: {
          animal: chosenAnimal,
          key: chosenOne,
          format,
        },
      };
    case "json":
      const json = JSON.parse(
        fs.readFileSync(`src/data/${chosenAnimal}/${chosenOne}/entry.json`)
      );

      json.id = chosenOne;
      json.uri = `/${chosenAnimal}/${chosenOne}`;

      const fileStats = fs.statSync(
        `src/data/${chosenAnimal}/${chosenOne}/entry.png`
      );
      json.size = `${fileStats.size / 1000} KB`;
      json.lastModified = fileStats.mtime;
      json.lastModifiedTimestamp = fileStats.mtimeMs;

      return {
        data: json,
        parameters: {
          animal: chosenAnimal,
          key: chosenOne,
          format,
        },
      };
    default:
      return {
        error: 400,
        message: "Invalid format",
        debug: {
          format,
        },
      };
  }
};
